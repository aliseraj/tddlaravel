<?php

namespace Tests\Feature\Models;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Tag;

class TagTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    use RefreshDatabase;

    public function testInsertData()
    {
        $data = Tag::factory()->make()->toArray();

        Tag::create($data);
        $this->assertDatabaseHas('tags', $data);
    }

    public function testTagRelationWithPost()
    {
        $count = rand(0,10);

        $tag = Tag::factory()->hasPosts($count)->create();

        $this->assertCount($count,$tag->posts);
        $this->assertTrue($tag->posts->first() instanceof Post);
    }
}
