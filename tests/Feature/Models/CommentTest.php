<?php

namespace Tests\Feature\Models;


use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Comment;

class CommentTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    use RefreshDatabase;

    public function testInsertData()
    {
        $data = Comment::factory()->make()->toArray();

        Comment::create($data);
        $this->assertDatabaseHas('comments', $data);
    }
}
